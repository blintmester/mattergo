package templateFunctions

import (
	"encoding/json"
	"text/template"
)

var Funcs = template.FuncMap{
	"escapeStr": func(str string) string {
		b, e := json.Marshal(str)
		if e != nil {
			panic(e)
		}
		return string(b)
	},
}
