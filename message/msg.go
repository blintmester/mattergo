package message

import (
	"github.com/mattermost/mattermost-server/v5/model"
	"gitlab.com/blintmester/mattergo/channel"
	"gitlab.com/blintmester/mattergo/user"
)

type DirectMsg struct {
	To      string `yaml:"to"`
	Message string `yaml:"message"`
}

type ChannelMsg struct {
	To      string `yaml:"to"`
	Team    string `yaml:"team"`
	Message string `yaml:"message"`
}

type Message struct {
	DirectMsg  *DirectMsg  `yaml:"direct"`
	ChannelMsg *ChannelMsg `yaml:"channel"`
}

func ChannelMessage(client *model.Client4, from *model.User, channel *model.Channel, message string) {
	SendMessage(client, channel, message)
}

func DirectMessage(client *model.Client4, from, to *model.User, message string) {
	dm, _ := client.CreateDirectChannel(from.Id, to.Id)
	SendMessage(client, dm, message)
}

func SendMessage(client *model.Client4, to *model.Channel, message string) {
	post := &model.Post{
		ChannelId: to.Id,
		Message:   message,
	}
	client.CreatePost(post)
}

func (msg *DirectMsg) Send(client *model.Client4, from *model.User) {
	to := user.GetUserByUsername(client, msg.To)
	DirectMessage(client, from, to, msg.Message)
	return
}

func (msg *ChannelMsg) Send(client *model.Client4, from *model.User) {
	to := channel.GetChannelByName(client, msg.Team, msg.To)
	ChannelMessage(client, from, to, msg.Message)
	return
}

func (msg *Message) Send(client *model.Client4, from *model.User) {
	if msg.DirectMsg != nil {
		msg.DirectMsg.Send(client, from)
	}

	if msg.ChannelMsg != nil {
		msg.ChannelMsg.Send(client, from)
	}
}
