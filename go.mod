module gitlab.com/blintmester/mattergo

replace gitlab.com/blintmester/mattergo => ./

go 1.16

require (
	github.com/dyatlov/go-opengraph v0.0.0-20210112100619-dae8665a5b09 // indirect
	github.com/mattermost/mattermost-server/v5 v5.34.2
	github.com/minio/minio-go/v7 v7.0.10 // indirect
	github.com/shibukawa/configdir v0.0.0-20170330084843-e180dbdc8da0 // indirect
	gitlab.com/MikeTTh/env v0.0.0-20210102155928-2e9be3823cc7
	golang.org/x/crypto v0.0.0-20210415154028-4f45737414dc // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
