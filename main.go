package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"github.com/mattermost/mattermost-server/v5/model"
	"github.com/shibukawa/configdir"
	"gitlab.com/blintmester/mattergo/message"
	"gitlab.com/blintmester/mattergo/templateFunctions"
	"golang.org/x/crypto/ssh/terminal"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"syscall"
	"text/template"
)

func plspanic() string { panic("EMAIL or PASSWORD env var is missing") }

type Config struct {
	Server   string `yaml:"server"`
	Email    string `yaml:"email"`
	Password string `yaml:"password"`
}

type MatterClient struct {
	Url       string
	ApiUrl    string
	AuthToken string
	AuthType  string
}

type MatterDetails struct {
	Client MatterClient
	UserId string
}

var configDirs = configdir.New("", "mattergo")

func main() {
	var dryRun = flag.Bool("dry-run", false, "do not send the messages, just show the yaml")
	flag.Parse()

	var details MatterDetails
	folder := configDirs.QueryFolderContainsFile("conf.yml")
	if folder != nil {
		data, e := folder.ReadFile("conf.yml")
		if e != nil {
			fmt.Printf("Config file does not exist: %s\n", folder.Path+"/conf.yml")
			details = firstRun()
		}
		e = yaml.Unmarshal(data, &details)
		if e != nil {
			panic(e)
		}
	} else {
		details = firstRun()
	}

	client, owner, resp := restoreClientAndOwner(details)
	if resp.Error != nil {
		var reader = bufio.NewReader(os.Stdin)
		fmt.Printf("We could not log you in, wanna try first run again? Y/n: ")
		res, e := reader.ReadString('\n')
		if e != nil {
			panic(e)
		}
		if strings.ToLower(res) == "y" || res == "" {
			client, owner, resp = restoreClientAndOwner(details)
			if resp.Error != nil {
				panic(resp.Error)
			}
		} else {
			os.Exit(1)
		}
	}

	var templateContext interface{}
	b, e := ioutil.ReadFile("values.yml")
	if e != nil {
		panic(e)
	}
	e = yaml.Unmarshal(b, &templateContext)
	if e != nil {
		panic(e)
	}

	files, e := ioutil.ReadDir("templates")
	if e != nil {
		panic(e)
	}
	var msg []message.Message
	for _, f := range files {
		var tmp []message.Message
		filename := "templates/" + f.Name()
		temp, e := template.New(f.Name()).Funcs(templateFunctions.Funcs).ParseFiles(filename)
		if e != nil {
			panic(e)
		}
		var buff bytes.Buffer
		e = temp.Execute(&buff, templateContext)
		if e != nil {
			panic(e)
		}
		e = yaml.Unmarshal(buff.Bytes(), &tmp)
		if e != nil {
			fmt.Println(buff.String())
			panic(e)
		}
		msg = append(msg, tmp...)
	}

	if *dryRun {
		b, e := yaml.Marshal(msg)
		if e != nil {
			panic(e)
		}
		fmt.Println(string(b))
		return
	}

	for _, m := range msg {
		m.Send(client, owner)
	}
}

func restoreClientAndOwner(details MatterDetails) (*model.Client4, *model.User, *model.Response) {
	var client = &model.Client4{
		Url:        details.Client.Url,
		ApiUrl:     details.Client.ApiUrl,
		AuthToken:  details.Client.AuthToken,
		AuthType:   details.Client.AuthType,
		HttpClient: http.DefaultClient,
	}
	owner, resp := client.GetUser(details.UserId, "")
	return client, owner, resp
}

func firstRun() MatterDetails {
	var conf Config
	var reader = bufio.NewReader(os.Stdin)
	fmt.Print("Please enter server URL: ")
	conf.Server, _ = reader.ReadString('\n')
	conf.Server = strings.TrimSpace(conf.Server)
	fmt.Print("Please enter your email address: ")
	conf.Email, _ = reader.ReadString('\n')
	conf.Email = strings.TrimSpace(conf.Email)
	fmt.Print("Please enter your password: ")
	bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
	if err == nil {
		conf.Password = string(bytePassword)
	}

	client := model.NewAPIv4Client(conf.Server)
	owner, resp := client.Login(conf.Email, conf.Password)
	if resp.Error != nil {
		panic(resp.Error.Error())
	}

	var details MatterDetails
	details.Client = MatterClient{
		Url:       client.Url,
		ApiUrl:    client.ApiUrl,
		AuthToken: client.AuthToken,
		AuthType:  client.AuthType,
	}
	details.UserId = owner.Id

	b, e := yaml.Marshal(details)
	if e != nil {
		panic(e)
	}

	folders := configDirs.QueryFolders(configdir.Global)
	e = os.MkdirAll(folders[0].Path, 0700)
	if e != nil {
		panic(e)
	}
	e = ioutil.WriteFile(folders[0].Path+"/conf.yml", b, 0600)
	if e != nil {
		panic(e)
	}
	return details
}
