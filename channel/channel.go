package channel

import "github.com/mattermost/mattermost-server/v5/model"

// http://your-mattermost-url.com/{team_name}/channels/{channel_name}
// example : https://mattermost.kszk.bme.hu/kszk/channels/test
// teamName: "kszk"
// channelName: "test"
func GetChannelByName(client *model.Client4, teamName, channelName string) *model.Channel {
	channel, _ := client.GetChannelByNameForTeamName(channelName, teamName, "")
	return channel
}
