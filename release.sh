#!/bin/bash -v

assets=()
while read -r f; do
  assets+=(" --assets-link {\"name\":\"$f\",\"url\":\"${PACKAGE_REGISTRY_URL}/$f\"}")
done < files.txt

release-cli create --name "Version $CI_COMMIT_TAG" --tag-name "$CI_COMMIT_TAG" $(printf "%b" "${assets[@]}")
