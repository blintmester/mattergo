# ![](https://assets.gitlab-static.net/uploads/-/system/project/avatar/26034508/mattergo_lil.png?width=32) MatterGo

## What's this?

This is essentially Helm for Mattermost messages. If you know how to use Helm, this should be
pretty straightforward for you.

It's a templating tool for those who want to send several, 
just slightly different messages on Mattermost.

## How to use

Create a `values.yml` file, than create a `templates` folder. Put yaml files into the 
`templates` folder. These files need to form a list following types:

```yaml
- direct:
    to: username
    message: message
```
or
```yaml
- channel:
    team: "team"
    to: "channel name" 
    message: "message"
```

These files will be templated using [Go templates](https://golang.org/pkg/text/template/),
the base context (`.`) of the templates is your `values.yml` file.

For an example, see the [example](example) folder.

Once you created your templates and your `values.yml`, simply run the program.
On the first run it will guide you through logging in, then send the messages you templated.

If you are interested in what the messages will look like, just use 
the `-dry-run` flag and the program will print the templated yaml.

## Download

You can download pre-built binaries from the [releases](https://gitlab.com/blintmester/mattergo/-/releases) page.
