package user

import "github.com/mattermost/mattermost-server/v5/model"

func GetUserByUsername(client *model.Client4, name string) *model.User {
	user, _ := client.GetUserByUsername(name, "")
	return user
}

func GetUserByEmail(client *model.Client4, email string) *model.User {
	user, _ := client.GetUserByEmail(email, "")
	return user
}
